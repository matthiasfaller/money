# -*- coding: utf-8-*-
import logging

from google.appengine.ext import db
from google.appengine.api import memcache
from google.appengine.api import users

class BaseModel(db.Model):
    @classmethod
    def all(cls, user_id):
        """
        Add ancestor filter to all() query

        Returns: Datastore Query including ancestor filter
        """
        q = super(BaseModel, cls).all()
        q.ancestor(cls.parent_key(user_id))

        return q

    @classmethod
    def by_id(cls, uid, user_id):
        return cls.get_by_id(int(uid), cls.parent_key(user_id))

    @classmethod
    def by_user_id(cls, user_id, order_by):
        q = cls.all(user_id)
        q.order(order_by)
        return q

    @classmethod
    def get_item(cls, item_id, user_id):
        """
        Gets a single item from memcache. If not cached, gets items from
        datastore and puts it into memcache.

        Returns: item
        """
        key = "item_" + item_id
        item = memcache.get(key)

        if not item:
            item = cls.by_id(item_id, user_id)
            logging.info("Query: {}.by_id({})".format(str(cls), item_id))
            memcache.set(key, item)

        return item

    @classmethod
    def delete_item(cls, item_id, user_id):
        """
        Deletes object from datastore.

        Returns: false if item does not exist
        """
        item = cls.by_id(item_id, user_id)
        if item:
            item.delete()
            logging.info("Delted entry with id {}".format(item_id))
            cls.clear_cache("item_" + item_id)
            return True

        logging.error("Not datastore item with given id {}".format(item_id))
        return False

    @staticmethod
    def parent_key(path):
        return db.Key.from_path("user_id_" + path, "entry")

    @staticmethod
    def get_items(key_str, user_id, q):
        """
        Gets key items from memcache. If not cached querries datastore and
        adds items to datastore.

        Returns: items
        """
        key = key_str + user_id
        items = memcache.get(key)

        if not items:
            #Get items from datastore and put them to memcache
            items = list(q.run())
            logging.info("Query: {}".format(str(q)))
            memcache.set(key, items)

        return items

    @classmethod
    def add_item(cls, user_id, **kw):
        """
        Add item to datastore

        Returns: item
        """
        item = cls(cls.parent_key(user_id), **kw)
        item.put()

        return item

    @classmethod
    def update_item(cls, user_id, item_id, **kw):
        """
        Updates item in datastore and deletes item cache

        Returns: item
        """
        item = cls.by_id(item_id, user_id)
        for key, value in kw.items():
            setattr(item, key, value)

        item.put()

        cls.clear_cache("item_" + item_id)

        return item


    @staticmethod
    def clear_cache(*keys):
        """
        Clears memcache of keys. Flushes complete cache if no key is provided
        """
        if keys:
            memcache.delete_multi(keys)
            logging.info("Flushed keys: " + str(keys))
        else:
            memcache.flush_all()
            logging.info("Complete memcache flushed")


class Category(BaseModel):
    name = db.StringProperty(required = True)
    updated = db.DateTimeProperty(auto_now = True)
    created = db.DateTimeProperty(auto_now_add = True)

    # Memcaches:
    # ----------
    # "categories_" + user_id: Liste alle Kategorien

    def get_dict(self):
        """
        Returns category as Python get_dict
        """
        return {"label": self.name}

    @classmethod
    def clear_category_cache(cls, user_id):
        """
        Clears all category related memcache keys
        """
        cls.clear_cache("categories_" + user_id)

    @classmethod
    def get_items(cls, user_id):
        """
        Returns all category objects from user_id
        """
        q = cls.by_user_id(user_id, "name")       
        return super(Category, cls).get_items("categories_", user_id, q)

    @classmethod
    def add_item(cls, user_id, **kw):
        """
        Add items to datastore and delete cache

        Returns: item
        """
        item = super(Category, cls).add_item(user_id, **kw)
        cls.clear_category_cache(user_id)

        return item

    @classmethod   
    def valid_category(cls, item_id, user_id):
        """
        Returns true if given category is a valid key. Valid means there is an datastore entry
        with this key
        """
        if cls.by_id(item_id, user_id):
            return True

        return False

    @classmethod
    def delete_item(cls, item_id, user_id):
        """
        Deletes item from datastore

        Returns: False if object does not exist
        """
        if super(Category, cls).delete_item(item_id, user_id):
            cls.clear_category_cache(user_id)
            return True

        return False


class Recipient(BaseModel):
    name = db.StringProperty(required = True)
    address = db.StringProperty()
    updated = db.DateTimeProperty(auto_now = True)
    created = db.DateTimeProperty(auto_now_add = True)

    # Memcaches:
    # ----------
    # "recipients_" + user_id: Liste alle Eintraege nach user_id
    # "item_" + entry_id: Einzelner Eintrag

    def get_dict(self):
        """
        Returns Recipient Object as Python Dictonary
        """
        return {"label": self.name, "address": self.address}

    @classmethod
    def clear_recipient_cache(cls, user_id):
        """
        Clears all recipient related memcache keys
        """
        cls.clear_cache("recipients_" + user_id, "recipients_names_" + user_id)

    @classmethod
    def unique_recipient(cls, user_id, name):
        """
        Returns True if recipients name is unique
        """
        q = cls.all(user_id)
        q.filter("name =", name)
        
        if q.count(limit = 1):
            return False
        return True

    @classmethod    
    def update_item(cls, user_id, item_id, **kw):
        """
        Updates existing item in datastore and deletes recipient cache

        Returns: item
        """
        item = super(Recipient, cls).update_item(user_id, item_id, **kw)
        cls.clear_recipient_cache(user_id)
        return item

    @classmethod
    def add_item(cls, user_id, **kw):
        """
        Add items to datastore and delete cache

        Returns: item
        """
        item = super(Recipient, cls).add_item(user_id, **kw)
        cls.clear_recipient_cache(user_id)
        return item

    @classmethod
    def get_items(cls, user_id):
        """
        Returns all recipient objects from user_id
        """
        q = Recipient.by_user_id(user_id, "name")       
        return super(Recipient, cls).get_items("recipients_", user_id, q)

    @classmethod
    def by_name(cls, user_id, name):
        """
        Returns recipient datastore object by name

        Returns: datastore object
        """
        q = cls.all(user_id)
        q.filter("name =", name)
        return q.get()

    @classmethod
    def delete_item(cls, item_id, user_id):
        """
        Deletes item from datastore

        Returns: False if object does not exist
        """
        if super(Recipient, cls).delete_item(item_id, user_id):
            cls.clear_recipient_cache(user_id)
            return True

        return False
      

class Entry(BaseModel):
    amount = db.FloatProperty(required = True)
    category = db.ReferenceProperty(Category)
    date = db.DateProperty(required = True)
    recipient = db.ReferenceProperty(Recipient)
    comment = db.TextProperty()
    updated = db.DateTimeProperty(auto_now = True)
    created = db.DateTimeProperty(auto_now_add = True)

    # Memcaches:
    # ----------
    # "entries_" + user_id: Liste alle Eintraege nach user_id
    # "item_" + entry_id: Einzelner Eintrag

    def get_dict(self):
        """
        Returns entry as python directory
        """
        d = {"amount": self.amount, "category": self.category.name, "recipient": "", "comment": self.comment, "date": self.date.strftime("%a %b %H:%M:%S %Y")}
        if self.recipient:
            d["recipient"] = self.recipient.name
        return  d

    @classmethod
    def clear_entry_cache(cls, user_id):
        """
        Clear entry related cache
        """
        cls.clear_cache("entries_" + user_id)

    @classmethod
    def get_items(cls, user_id):
        """
        Returns all recipient objects from user_id
        """
        q = cls.by_user_id(user_id, "date")       
        return super(Entry, cls).get_items("entries_", user_id, q)

    @classmethod
    def add_item(cls, user_id, **kw):
        """
        Add items to datastore and delete cache

        Returns: item
        """
        item = super(Entry, cls).add_item(user_id, **kw)
        cls.clear_entry_cache(user_id)
        return item

    @classmethod    
    def update_item(cls, user_id, item_id, **kw):
        """
        Updates existing item in datastore and deletes recipient cache

        Returns: item
        """
        item = super(Entry, cls).update_item(user_id, item_id, **kw)
        cls.clear_entry_cache(user_id)
        return item

    @classmethod
    def delete_item(cls, item_id, user_id):
        """
        Deletes item from datastore

        Returns: False if object does not exist
        """
        if super(Entry, cls).delete_item(item_id, user_id):
            cls.clear_entry_cache(user_id)
            return True

        return False