# -*- coding: utf-8-*-
import webapp2
import logging
import cgi
import datetime
import jinja2
import os
import json

import utils

import models

from google.appengine.api import users

template_dir = os.path.join(os.path.dirname(__file__), "templates")
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir),
                                autoescape = True)

# ------------
# Jinja Filter
# ------------
def dateformat(val, format = "%d.%m.%Y"):
    return val.strftime(format)

jinja_env.filters['dateformat'] = dateformat

def currencyformat(val, sep = ",", unit = " &euro;"):
    s = "{0:.2f}".format(val)
    s = "{}{}".format(s.replace(".", sep), unit)
    return s

jinja_env.filters['currencyformat'] = currencyformat

def logout_url():
    return users.create_logout_url("/")

jinja_env.globals['logout_url'] = logout_url

def login_url():
    return users.create_login_url("/")

jinja_env.globals['login_url'] = login_url


# ----------
# Decorators
# ----------
def require_login(funct):
    """
    Decorate with this function to limit access to logged in users
    """
    def logged_in(self, *a, **kw):
        if not self.user:
            if self.request.method == "GET":
                self.redirect(login_url())
                return
            self.error(403)
        else:
            return funct(self, *a, **kw)
    return logged_in


class BaseHandler(webapp2.RequestHandler):
    """
    Basic methodes shared by all handlers

    Attributes:
        user: App engine user object from currently logged in user_id
        user_id: Id of currently logged in user_id
        format: json/html json is url ends with .json
    """
    
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **parameter):
        t = jinja_env.get_template(template)
        return t.render(parameter)

    def render_json(self, s):
        self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
        self.write(json.dumps(s))

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

    def initialize(self, *a, **kw):
        """Check if user is logged in an store user object in self.user"""
        #https://developers.google.com/appengine/docs/python/tools/webapp/requesthandlerclass#RequestHandler_initialize
        #Die Funktion wird vor allen anderem bei jedem Request aufgerufen
        #Wenn der User angemeldet ist, wird sein in der variable user gespeicher
        webapp2.RequestHandler.initialize(self, *a, **kw)

        self.user = users.get_current_user()
        if self.user: 
            self.user_id = self.user.user_id()

        self.format = ("json" if self.request.url.endswith('.json') else "html")


class MainHandler(BaseHandler):
    @require_login
    def get(self):
        entries = models.Entry.get_items(self.user_id)
        self.render("index.html", entries = entries)


class AddEntry(BaseHandler):
    def render_form(self, *a, **kw):
        """
        Renders edit form
        """
        self.render("add.html", **kw)

    @require_login
    def get(self, param = None):
        categories = models.Category.get_items(self.user_id)

        if param:
            #With parameter. Load entry for editing
            entry = models.Entry.get_item(param, self.user_id)
            if entry:
                recipient = (entry.recipient.name if entry.recipient else "")
                self.render_form(amount_val = entry.amount, 
                                 date = datetime.datetime.strftime(entry.date, "%d.%m.%Y"), 
                                 categories = categories, 
                                 recipient_val = recipient, 
                                 comment_val = entry.comment,
                                 category_val = entry.category.key().id()
                                )
            else:
                logging.error("Nutzer hat eine falsche ID für einen Eintrag zum editieren übergeben")
                self.redirect("/")
                return False
        else:
            #Without parameter, render empty form  
            date = datetime.date.today().strftime("%d.%m.%Y")
            self.render_form(date = date, categories = categories)

    @require_login
    def post(self, param = None):
        amount_str = cgi.escape(self.request.get("amount"))
        date_str = cgi.escape(self.request.get("date"))

        category_id_str = cgi.escape(self.request.get("category"))

        recipient_str = cgi.escape(self.request.get("recipient")) 
        comment_str = cgi.escape(self.request.get("comment"))

        if not(amount_str and category_id_str and date_str):
            logging.error("Missing data")
            return False
            self.redirect("/")

        if not(utils.valid_Number(amount_str) and utils.valid_Date(date_str) and models.Category.valid_category(category_id_str, self.user_id)):
            logging.error("Submitted data not valid")
            return False
            self.redirect("/")

        amount = float(amount_str)
        date = datetime.datetime.strptime(date_str, "%d.%m.%Y").date()
        category = models.Category.by_id(category_id_str, self.user_id)

        if recipient_str:
            recipient = models.Recipient.by_name(self.user_id, recipient_str)
            if not recipient:
                recipient = models.Recipient.add_item(self.user_id, name = recipient_str)
        else:
            recipient = None

        if param:
            models.Entry.update_item(self.user_id, param, category = category, recipient = recipient, amount = amount, date = date, comment = comment_str)
        else:
            models.Entry.add_item(self.user_id, category = category, recipient = recipient, amount = amount, date = date, comment = comment_str)
            
        self.redirect("/")
        

class DeleteEntry(BaseHandler):
    @require_login
    def get(self, param):
        if models.Entry.delete_item(param, self.user_id):
            self.redirect("/")
        else:
            logging.error("Wrong id to delete submitted")


class CSVHandler(BaseHandler):
    @require_login
    def get(self):
        entries = models.Entry.get_items(self.user_id)

        s = '"{}";"{}";"{}";"{}";"{}"\n'.format("Betrag", "Kategorie", "Datum", "Empfänger", "Kommentar")

        for e in entries:
            recipient_str = (e.recipient.name if e.recipient else "")
            s += '"-{}";"{}";"{}";"{}";"{}"\n'.format(str(e.amount).replace(".", ","), e.category.name.encode('UTF-8'), e.date, recipient_str, e.comment.encode("utf-8"))

        self.response.headers['Content-Type'] = 'text/plain; charset=UTF-8'
        self.write(s)


class CategoryHandler(BaseHandler):
    def render_form(self, **kw):
        """
        Renders category page
        """    
        categories = models.Category.get_items(self.user_id)
        self.render("category.html", categories = categories, **kw)

    @require_login
    def get(self):
        self.render_form()

    @require_login
    def post(self):
        name_str = cgi.escape(self.request.get("name"))

        if not name_str:
            logging.error("Missing data")
            return False

        models.Category.add_item(self.user_id, name = name_str)
        self.redirect("/category")


class DeleteCategory(CategoryHandler):
    @require_login
    def get(self, param):
        category = models.Category.by_id(param, self.user_id)

        if category.entry_set.count(limit = 1):
            error_msg = "In Verwendung, kann nicht geloescht werden"
            self.render_form(error_msg = error_msg)
        else:
            models.Category.delete_item(param, self.user_id)
            self.redirect("/category")


class EntryHandler(BaseHandler):
    @require_login
    def get(self, param):
        entry = models.Entry.get_item(param, self.user_id)
        if entry:
            self.render_json(entry.get_dict())
        else:
            self.render_json({"error": "no entry"}) 


class RecipientHandler(BaseHandler):
    def render_form(self, *a, **kw):
        """
        Renders edit form
        """
        recipients = models.Recipient.get_items(self.user_id)

        self.render("recipients.html", recipients = recipients, **kw)

    @require_login
    def get(self, param = None):
        if param:
            recipient = models.Recipient.get_item(param, self.user_id)
            if recipient:
                self.render_json(recipient.get_dict())
            else:
                self.render_json({"error": "no entry"})
        else:
            if self.format == "json":
                recipients = models.Recipient.get_items(self.user_id)
                s = [r.get_dict() for r in recipients]
                self.render_json(s)
            else:           
                self.render_form()

    @require_login                
    def post(self):
        name_str = cgi.escape(self.request.get("name"))
        address_str = cgi.escape(self.request.get("address"))
        item_id_str = cgi.escape(self.request.get("item_id"))

        if not name_str:
            logging.error("User hat keine Daten eingegeben")
            return False            
  
        if item_id_str:
            #Update entry
            models.Recipient.update_item(self.user_id, item_id_str, name = name_str, address = address_str)
        else:
            #New Entry
            if not models.Recipient.unique_recipient(self.user_id, name_str):
                #No unique name
                self.render_form(name_val = name_str,
                                 address_val = address_str,
                                 item_id_val = item_id_str,
                                 error_msg = "Bitte einen eindeutigen Namen eingeben"
                                 )
                return False

            models.Recipient.add_item(self.user_id, name = name_str, address = address_str)
        
        self.redirect("/recipient")


class DeleteRecipient(RecipientHandler):
    @require_login
    def get(self, param):
        recipient = models.Recipient.by_id(param, self.user_id)
        if recipient.entry_set.count(limit = 1):
            error_msg = "In Verwendung, kann nicht geloescht werden"
            self_render_form(error_msg = error_msg)
        else:
            models.Recipient.delete_item(param, self.user_id)
            self.redirect("/recipient")