# -*- coding: utf-8-*-
import logging
import re

DATE_RE = re.compile(r"^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$")
NUMBER_RE = re.compile(r"^[-+]?[0-9]*[,\.]?[0-9]+$")

def valid_Date(s):
    """Returns true is string is a valid date of format yyyy-mm-dd"""
    return DATE_RE.match(s)

def valid_Number(s):
    """Returns true if string is a valid number"""
    return NUMBER_RE.match(s)

class ClassName(object):
    """docstring for ClassName"""
    def __init__(self, arg):
        super(ClassName, self).__init__()
        self.arg = arg
        