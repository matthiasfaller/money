#Money
##Beschreibung
Mit Money können Bargeldausgaben unterwegs schnell aufgezeichnet werden. Es kann ein Kategorieset angelegt werden, dem die Einträge zugeordnet werden. Empfänger der Zahlungen werden bei der ersten Verwendung der Liste der Empfänger hinzugefügt und stehen ab dann in per autocomplete für neue Einträge zur Verfügung. Daten können per csv exportiert werden, um in die Desktopfinanzverwaltung importiert werden.

##Hintergrund
Money ist für mich als Ergänzung zu Starmoney für Mac entstanden. Ich habe Bargeldausgaben nie aufgezeichnet, weil es unterweg zu umständlich war. Gleichzeitig habe ich keine iOS App gefunden, die nur Aufzeichnet und keine voll Finanzverwaltung ist. Notieren mit einer der Notizapps erfordert nachträglich editieren und geht nicht schnell genug. In Money wird der Betrag eingegeben und eine Kategorie aus dem Dropdown gewählt. Das Kategorieset besteht aus dem, welches auch für die persönliche Finanzeverwaltung verwendet wird. Money ist auch ein Übungsprojekt um meine Programmierkentnisse weiter zu vertiefen.

Source code: [Bitbucket Repository](https://bitbucket.org/matthiasfaller/money)

System ist im Moment eine closed beta und erfordert einen Google Account. Wer trotzdem testen will, kann mich gerne über [Twitter](http://www.twitter.com/matthiasfaller) ansprechen.

##Technologie
Money ist in Python entwickelt und läuft auf der [Google App Engine](https://developers.google.com/appengine/). Zur Google App Engine bin ich über den [Udacity Kurs](https://www.udacity.com/course/cs253) gekommen. Ich möchte mich zunächst mit der Applikation und nicht mit dem Server beschäftigen.Die Oberfläche ist in HTML/CSS, Javascipt und [jQuery](https://jquery.com/) erstellt.

##Credits

Icons: [Font Awesome](https://fortawesome.github.io/Font-Awesome/)
App icons: [VisualPharm](http://icons8.com/)