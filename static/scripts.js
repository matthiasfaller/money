function confirm_delete(selector) {
    /*
    Add confirm delete to click event of selector
    */
    $(selector).click(function(event) {
        // /* Act on the event */
        if (!confirm("Wirklich löschen?")) {
            event.preventDefault();
        }          
    });     
}

function generate_recipient_html(data) {
    /*
    Return HTML for recipient datails row
    */
    return "<tr class='datatable-row datatable-row-skin details_section" + "'>\n <td class='datatable-data datatable-data-skin' colspan='3'>\n <strong>Adresse: </strong>\n" + data.address + "\n </td>\n </tr>";
}

function generate_entry_html(data) {
    /*
    Returns HTML for entry details row
    */
    return "<tr class='datatable-row datatable-row-skin details_section" + "'>\n <td class='datatable-data datatable-data-skin' colspan='5'>\n <strong>Empf&auml;nger: </strong>\n" + data.recipient + "<br>\n <strong>Kommentar: </strong>\n" + data.comment + "<br>\n </td>\n </tr>";
}

function show_details(event, path, html_generator) {
    /*
    Add detailes line below table row where event is happening. User html_generator
    to generate needed HTML
    */         
    console.log(event)
    //Element der Zeile direct ueber dem Icon Link
    var e = $(event.target).parents(".datatable-row");

    //Sichtbarkeit der Icon tauschen
    $(e).find(".showdetails .fa").toggle();

    //Wenn direct unter der Datenzeil keie Zeile mit den Details ist, dies anzeigen
    if ($(e).next(".details_section").length == 0) {    
        var url = path + e.attr('id');
        $.getJSON(url, function(data) {
            $(e).after(html_generator(data))
        });
    } else {
        // Wenn schon eine Zeile anzeigt wird. Die direkt auf die Datenziel folgende Zeile entfernen
        $(e.next(".details_section")).remove();
    };  
 
}