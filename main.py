#!/usr/bin/env python
# -*- coding: utf-8-*-
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import logging

import handlers

app = webapp2.WSGIApplication([
    ('/', handlers.MainHandler),
    ('/add', handlers.AddEntry),
    ('/csv', handlers.CSVHandler),
    ('/add/(\d+)?', handlers.AddEntry),
    ('/_del/(\d+)', handlers.DeleteEntry),
    ('/category', handlers.CategoryHandler),
    ('/category/_del/(\d+)', handlers.DeleteCategory),
    ('/entry/(\d+)(?:.json)?', handlers.EntryHandler),
    ('/recipient(?:.json)?', handlers.RecipientHandler),
    ('/recipient/(\d+)?', handlers.RecipientHandler),
    ('/recipient/_del/(\d+)?', handlers.DeleteRecipient),
], debug=True)
